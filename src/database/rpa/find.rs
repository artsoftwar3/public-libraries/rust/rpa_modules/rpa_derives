/**
    Rpa (Rust Persistence API) Derive Find Partial implementation.
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

use quote::quote;

pub fn get_impl(type_name: &syn::Ident, table_name: &syn::Ident) -> proc_macro2::TokenStream {
    quote! {
        fn find(entity_id: &String, connection: &DatabaseConnection) -> Result<#type_name, RpaError> {
            let error_message: String = format!("No {} was found with id {}", stringify!(#type_name), entity_id);
            let result = #table_name::table.find(entity_id).get_result(connection);
            RpaError::map_result::<#type_name>(error_message, result)
        }
        fn exists(entity_id: &String, connection: &DatabaseConnection) -> Result<bool, RpaError> {
            let error_message: String = format!("An error has occurred trying to find a {} with id {}", stringify!(#type_name), entity_id);
            let result: Result<#type_name, Error> = #table_name::table.find(entity_id).first(connection);
            if result.is_err() {
                let result_error = result.err().unwrap();
                if result_error == Error::NotFound {
                    return Ok(false);
                } else {
                    return RpaError::build_from::<bool>(error_message, result_error);
                }
            }
            Ok(true)
        }
        fn find_all(connection: &DatabaseConnection) -> Result<Vec<#type_name>, RpaError> {
            let error_message: String = format!("No {} was found", stringify!(#type_name));
            let result = #table_name::table.order(#table_name::id).load::<#type_name>(connection);
            RpaError::map_result::<Vec<#type_name>>(error_message, result)
        }
    }
}