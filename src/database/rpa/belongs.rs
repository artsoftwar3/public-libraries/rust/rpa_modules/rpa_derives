/**
    Rpa (Rust Persistence API) Derive Belongs Partial implementation.
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

use syn::Ident;
use proc_macro2::Span;
use quote::quote;

pub fn get_impl(type_name: &syn::Ident, belongs: &Vec<String>) -> Vec<proc_macro2::TokenStream> {
    let mut belongs_impl: Vec<proc_macro2::TokenStream> = Vec::new();

    for belong_string in belongs {
        let method_name = Ident::new((String::from("find_for_") + &belong_string.to_lowercase()).as_str(), Span::call_site());
        let belong: Ident = Ident::new(belong_string.as_str(), Span::call_site());
        belongs_impl.push(quote! {
            pub fn #method_name(parents: &Vec<#belong>, connection: &DatabaseConnection) -> Result<Vec<#type_name>, RpaError> {
                let result = #type_name::belonging_to(parents)
                    .load::<#type_name>(connection);
                let error_message: String = format!("A problem has occurred finding {} for {}", stringify!(#type_name), stringify!(#belong));
                RpaError::map_result::<Vec<#type_name>>(error_message, result)
            }
        });
    }

    belongs_impl
}