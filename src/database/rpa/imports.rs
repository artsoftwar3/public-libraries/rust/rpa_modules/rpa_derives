/**
    Rpa (Rust Persistence API) Derive Imports Partial implementation.
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

use quote::quote;
use crate::generic::connection_from_string;

pub fn get_impl(connection_type: &String, table_name: &syn::Ident) -> proc_macro2::TokenStream {
    let connection: proc_macro2::TokenStream = connection_from_string(&connection_type);
    quote! {
        use diesel;
        use diesel::prelude::*;
        use diesel::associations::Identifiable;
        use diesel::result::Error;
        use serde::{Deserialize, Serialize};
        use rocket_contrib::json::Json;
        use rpa::{RpaError, SearchRequest, SearchResponse, do_search};
        use uuid::Uuid;
        use crate::schema::#table_name;
        #connection as DatabaseConnection;
    }
}