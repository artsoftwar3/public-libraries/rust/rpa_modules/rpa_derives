/**
    Rpa (Rust Persistence API) Derive Save Partial implementation.
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

use quote::quote;

pub fn get_impl(type_name: &syn::Ident, table_name: &syn::Ident) -> proc_macro2::TokenStream {
    quote! {
        fn save(entity: &#type_name, connection: &DatabaseConnection) -> Result<#type_name, RpaError> {
            let error_message: String = format!("Error creating the new {}", stringify!(#type_name));
            let mut entity_id: String = entity.id.clone();
            if entity_id.is_empty() {
               entity_id = Uuid::new_v4().to_string();
            }
            let entity_to_save: #type_name = #type_name{
                id: entity_id.clone(),
                ..entity.clone()
            };
            let result_save = diesel::insert_into(#table_name::table)
                .values(entity_to_save)
                .execute(connection);
            if result_save.is_err() {
                let result_error = result_save.err().unwrap();
                return RpaError::build_from::<#type_name>(error_message, result_error);
            }
            let result_find = #table_name::table.find(entity_id).get_result(connection);
            if result_find.is_err() {
                let result_error = result_find.err().unwrap();
                return RpaError::build_from::<#type_name>(error_message, result_error);
            }
            RpaError::map_result::<#type_name>(error_message, result_find)
        }
        fn save_self(self: Self, connection: &DatabaseConnection) -> Result<#type_name, RpaError> {
            let error_message: String = format!("Error creating the new {}", stringify!(#type_name));
            let mut entity_id: String = self.id.clone();
            if entity_id.is_empty() {
               entity_id = Uuid::new_v4().to_string();
            }
            let entity_to_save: #type_name = #type_name{
                id: entity_id.clone(),
                ..self.clone()
            };
            let result_save = diesel::insert_into(#table_name::table)
                .values(&self)
                .execute(connection);
            if result_save.is_err() {
                let result_error = result_save.err().unwrap();
                return RpaError::build_from::<#type_name>(error_message, result_error);
            }
            let result_find = #table_name::table.find(entity_id).get_result(connection);
            if result_find.is_err() {
                let result_error = result_find.err().unwrap();
                return RpaError::build_from::<#type_name>(error_message, result_error);
            }
            RpaError::map_result::<#type_name>(error_message, result_find)
        }
        fn save_batch(entities: Vec<#type_name>, connection: &DatabaseConnection) -> Result<usize, RpaError> {
            let error_message: String = format!("Error creating the new entities of type {}", stringify!(#type_name));
            let mut entities_to_save: Vec<#type_name> = Vec::new();
            for entity in entities {
                let mut entity_id = entity.id.clone();
                if entity_id == String::new() {
                   entity_id = Uuid::new_v4().to_string();
                }
                let entity_to_save: #type_name = #type_name{
                    id: entity_id.clone(),
                    ..entity.clone()
                };
                entities_to_save.push(entity_to_save);
            }
            let result_save = diesel::insert_into(#table_name::table)
                .values(entities_to_save)
                .execute(connection);
            if result_save.is_err() {
                let result_error = result_save.err().unwrap();
                return RpaError::build_from::<usize>(error_message, result_error);
            }
            RpaError::map_result::<usize>(error_message, result_save)
        }
    }
}