#![recursion_limit="256"]
/**
    Rpa (Rust Persistence API) Derive definition
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

extern crate proc_macro;
extern crate syn;

use crate::proc_macro::TokenStream;

mod database;
mod generic;
use database::rpa;
use generic::derive_macro;

#[proc_macro_derive(Rpa, attributes(connection_type))]
pub fn derive_rpa(tokens: TokenStream) -> TokenStream {
    derive_macro(tokens, rpa::impl_rpa)
}

